import torch

device = (
    "cuda"
    if torch.cuda.is_available()
    else "mps"
    if torch.backends.mps.is_available()
    else "cpu"
)


from .model import AgeIdentifier
from .preprocess import preprocess
from .training import run
from voice_gender_recognition.colorcli import log

from tqdm import tqdm
import pandas as pd


@log("Removing nan and other genders")
def remove_empty(data: pd.DataFrame):
    new_data = pd.DataFrame({"filename": [], "age": [], "gender": []})

    cnt = 0
    for it in tqdm(data[["path", "age", "gender"]].values):
        if isinstance(it[2], str) and isinstance(it[1], str) and it[2] != "other":
            new_data.loc[cnt] = it
            cnt += 1

    print("Number of entries:", cnt)
    return new_data


@log("Merging classes")
def merge_classes(data: pd.DataFrame):
    convert = {
        "teens": "teenager",
        "twenties": 'young',
        "thirties": 'young',
        "fourties": 'middle',
        "fifties": 'middle',
        "sixties": 'elderly',
        "seventies": 'elderly',
        "eighties": 'elderly',
        "nineties": 'elderly',
        "teenager": "teenager",
        "young": "young",
        "middle": "middle",
        "elderly": "elderly"
    }
    for i in tqdm(range(data.shape[0])):
        data.iloc[i, 1] = convert[data.iloc[i, 1]]
    return data


def count_data(data):
    cnt = {
        "teenager": 0,
        "young": 0,
        "middle": 0,
        "elderly": 0
    }
    cnt_gender = {
        "male": 0,
        "female": 0
    }
    for it in data.values:
        cnt[it[1]] += 1
        cnt_gender[it[2]] += 1

    return {"gender": cnt_gender, "age": cnt}


def save_data(location, data):
    for id, x in enumerate(data):
        x.to_csv(location + "/" + ["train", "test", "dev"][id] + ".tsv", sep="\t")


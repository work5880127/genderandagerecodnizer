import torch
from torch import nn
from torchsummary import summary


class AgeIdentifier(nn.Module):
    def __init__(self, dropout=0):
        super().__init__()

        self.struct = nn.Sequential(
            nn.Linear(23, 16),
            nn.ReLU(),
            nn.BatchNorm1d(1),
            nn.Dropout(dropout),

            nn.Linear(16, 16),
            nn.ReLU(),
            nn.BatchNorm1d(1),
            nn.Dropout(dropout),

            nn.Linear(16, 4),
        )

    def forward(self, x):
        res = self.struct(x)

        # removing one unnecessary dimension
        res = res.reshape((res.size()[0], res.size()[-1]))
        return res


if __name__ == "__main__":
    print(summary(AgeIdentifier()))

import os
import shutil
from math import inf

from torch import nn
import torch
import matplotlib.pyplot as plt
from torch.utils.data import DataLoader
from tqdm import tqdm
from decouple import config

from voice_gender_recognition.gender_recognizer import GenderIdentifier
from voice_gender_recognition.gender_recognizer.preprocess import OpenVoiceDataset
from voice_gender_recognition.preprocess import Scaler
from voice_gender_recognition.gender_recognizer import device


def save_checkpoint(state, number, is_best, path="./model_params", **params):
    filename = os.path.join(path, f'checkpoint({number}).pth')
    if not os.path.exists(path):
        print("Checkpoint Directory does not exist. Creating it")
        os.mkdir(path)
    torch.save(state, filename)
    if is_best:
        shutil.copyfile(filename, os.path.join(path, f"model_best_.pth"))


def plot_losses(train_losses, val_losses, epoch, check_freq, epoch_size, train_x):
    plt.close()
    plt.figure()  # figsize=(5, 2)
    plt.plot(train_x, train_losses[1:], label='Train loss')
    #plt.plot(list(range(epoch_size + 1, epoch_size * (epoch + 1) + 1, epoch_size)), val_losses[1:],
    #         label='Validation loss')
    plt.plot(train_x, val_losses[1:], label='Validation loss')
    plt.title('Model loss')
    plt.ylim(0, 1)
    plt.legend()
    plt.ion()
    plt.show()
    plt.pause(0.001)


def train_loop(dataloader, testing_dataloader, model, loss_fn, optimizer, current_epoch, check, data, data_times,
               test_data):
    size = len(dataloader.dataset)
    # Set the model to training mode - important for batch normalization and dropout layers
    # Unnecessary in this situation but added for best practices
    model.train()

    for batch, (X, y) in enumerate(tqdm(dataloader,
                                        desc=f"Epoch {current_epoch}",
                                        leave=False)):
        # Compute prediction and loss
        pred = model(X)
        loss = loss_fn(pred, y)

        # Backpropagation
        loss.backward()
        optimizer.step()
        optimizer.zero_grad()

        if batch % check == 0:
            loss, current = loss.item(), (batch + 1) * len(X) + (current_epoch - 1) * size
            is_best = False
            if loss < min(data):
                is_best = True
            save_checkpoint(model.state_dict(), current, is_best, path=config("AGE_MODEL_CHECKPOINT_DIR"))
            data.append(loss)
            data_times.append(current)
            test_data.append(test_loop(testing_dataloader, model, loss_fn))


def test_loop(dataloader, model, loss_fn, out=False):
    # Set the model to evaluation mode - important for batch normalization and dropout layers
    # Unnecessary in this situation but added for best practices
    model.eval()
    size = len(dataloader.dataset)
    num_batches = len(dataloader)
    test_loss, correct = 0, 0

    # Evaluating the model with torch.no_grad() ensures that no gradients are computed during test mode
    # also serves to reduce unnecessary gradient computations and memory usage for tensors with requires_grad=True
    with torch.no_grad():
        X, y = next(iter(dataloader))
        pred = model(X)
        test_loss += loss_fn(pred, y).item()
        correct += (pred.argmax(1) == y.argmax(1)).type(torch.float).sum().item()
    correct /= len(X)
    if out:
        print(f"Test Error: \n Accuracy: {(100*correct):>0.1f}%, Avg loss: {test_loss:>8f} \n")
    return test_loss


def run(num_epoch, learning_rate, check_rate, weight_decay=0.0001, dropout=0):
    scaler = Scaler.from_file(config("GENDER_DATASET") + "/normalizer_config.txt")
    train_dataset = OpenVoiceDataset(config("GENDER_DATASET") + "/train.tsv", scaler)
    dev_dataset = OpenVoiceDataset(config("GENDER_DATASET") + "/dev.tsv", scaler)

    train_dataloader = DataLoader(train_dataset, batch_size=256, shuffle=True)
    dev_dataloader = DataLoader(dev_dataset, batch_size=200, shuffle=True)

    model = GenderIdentifier(dropout=dropout).to(device)
    loss_fn = nn.BCELoss()
    optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate, weight_decay=weight_decay)
    train_loss = [inf]
    train_times = []
    test_loss = [inf]

    for t in range(num_epoch):
        train_loop(train_dataloader, dev_dataloader, model, loss_fn, optimizer, t + 1, check_rate, train_loss, train_times, test_loss)
        test_loop(dev_dataloader, model, loss_fn, True)
        plot_losses(train_loss, test_loss, t + 1, check_rate, len(train_dataloader.dataset), train_times)

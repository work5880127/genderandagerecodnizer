import time

import pandas as pd
from decouple import config
from torch.utils.data import Dataset
import torch
from torchvision.transforms import Lambda
import numpy as np
from tqdm import tqdm

from voice_gender_recognition.colorcli import log
from voice_gender_recognition.preprocess import strip_read_csv, count_data, cleanup_data, load_all, for_all, save_data
from voice_gender_recognition.preprocess.features import create_df_features


class OpenVoiceDataset(Dataset):
    def __init__(self, path, scaler):
        super().__init__()
        self.header = strip_read_csv(path, sep='\t')
        self.label_transform = Lambda(lambda y: torch.zeros(4, dtype=torch.float).scatter_(0, torch.tensor(y), 1.0))
        self.scaler = scaler

    @staticmethod
    def to_num(label):
        convert = {
            "teenager": 0,
            "young": 1,
            "middle": 2,
            "elderly": 3
        }
        return convert[label]

    def __len__(self):
        return len(self.header)

    def __getitem__(self, item):
        label = self.header.iloc[item, 0]
        label = self.to_num(label)
        label = self.label_transform(label)
        data = self.header.iloc[item, 1:]

        # converting to tensor
        data = np.vstack(data.values).astype(np.float32)
        data = torch.from_numpy(data.transpose())
        data = self.scaler.scale(data)

        return data, label


def preprocess(cleanup=True):
    if cleanup:
        cleanup_data()
    data = load_all(config("DATASET_HEADERS"))
    data = for_all(data, lambda x: create_df_features(x, label="age"))
    save_data(config("AGE_DATASET"), data)

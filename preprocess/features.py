import librosa
from decouple import config
from tqdm import tqdm
import numpy as np
import pandas as pd
from voice_gender_recognition.colorcli import log


def samples_to_features(audio: np.ndarray, sampling_rate, inplace=None):
    """
    Extracting features from a librosa representation
    :param audio: contains the sampled track
    :param sampling_rate: sampling rate used to sample the file
    :param inplace: provide a list here if you want resulting features to extend an existing list
    :return: a list of features describing the file
    """
    res = []
    if inplace is not None:
        res = inplace
    audio, _ = librosa.effects.trim(audio)
    spectral_centroid = np.mean(librosa.feature.spectral_centroid(y=audio, sr=sampling_rate))
    spectral_bandwidth = np.mean(librosa.feature.spectral_bandwidth(y=audio, sr=sampling_rate))
    spectral_rolloff = np.mean(librosa.feature.spectral_rolloff(y=audio, sr=sampling_rate))
    res.append(spectral_centroid)
    res.append(spectral_bandwidth)
    res.append(spectral_rolloff)
    mfcc = librosa.feature.mfcc(y=audio, sr=sampling_rate)
    for el in mfcc:
        res.append(np.mean(el))

    return res


def feature_extraction(filename, data, sampling_rate=8000, label_name="gender"):
    """
    :param label_name: which labes describes this audio in the database
    :param filename: name of a file to analyze
    :param data: dataset with labels
    :param sampling_rate: recording frequency
    :return:
    """
    path = "{}{}".format(config("DATASET_LOCATION") + "/clips/", filename)
    features = list()
    label = data[data['filename'] == filename][label_name].values[0]
    features.append(label)

    audio, _ = librosa.load(path, sr=sampling_rate)
    samples_to_features(audio, sampling_rate, features)

    return features


@log("Analyzing audio files")
def create_df_features(orig, label="gender"):
    """
    :param label: dataset label column name
    :param orig: dataset with filenames and labels
    :return: dataframe with features and labels
    """
    new_rows = list()

    for idx, row in tqdm(orig.iterrows(), total=len(orig)):
        features = feature_extraction(row['filename'], orig, label_name=label)
        new_rows.append(features)

    return pd.DataFrame(new_rows, columns=["label", "spectral_centroid", "spectral_bandwidth", "spectral_rolloff",
                                           "mfcc1", "mfcc2", "mfcc3", "mfcc4", "mfcc5", "mfcc6", "mfcc7", "mfcc8",
                                           "mfcc9", "mfcc10", "mfcc11", "mfcc12", "mfcc13", "mfcc14", "mfcc15",
                                           "mfcc16",
                                           "mfcc17", "mfcc18", "mfcc19", "mfcc20"])

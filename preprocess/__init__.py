from voice_gender_recognition.colorcli import log
from .manipulations import *
from .preprocess import *
from .scaler import Scaler


@log("Preprocessing...")
def cleanup_data():
    ds = for_all(None, remove_empty)
    ds = for_all(ds, merge_classes)
    save_data(config('DATASET_HEADERS'), ds)

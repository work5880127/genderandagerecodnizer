torch~=2.3.0+rocm5.7
librosa~=0.10.1
Flask~=3.0.3
numpy~=1.24.4
pandas~=2.0.3
tqdm~=4.66.1
matplotlib~=3.7.2
torchvision~=0.18.0+rocm5.7
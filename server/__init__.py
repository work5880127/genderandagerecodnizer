import torch
from flask import Flask
from voice_gender_recognition.gender_recognizer import GenderIdentifier
from voice_gender_recognition.age_recognizer import AgeIdentifier
from voice_gender_recognition.preprocess import scaler
import librosa
from flask import request
from decouple import config
import base64

from voice_gender_recognition.preprocess.features import samples_to_features


app = Flask(__name__)
model = GenderIdentifier()
model.load_state_dict(torch.load(config("GENDER_MODEL_WEIGHTS")))
model.eval()
age_model = AgeIdentifier()
age_model.load_state_dict(torch.load(config("AGE_MODEL_WEIGHTS")))
age_model.eval()
scaler = scaler.Scaler.from_file(config("SCALER_PATH"))


@app.post("/inference")
def inference():
    if request.content_length > int(config("CONTENT_LENGTH_THRESHOLD")):
        return "file is too long", 400

    try:
        file = open("temp_from_base64.mp3", "bw")
        file.write(base64.b64decode(request.get_json()))
        file.close()
        audio, _ = librosa.load("temp_from_base64.mp3", sr=8000)
        features = torch.tensor([[samples_to_features(audio, 8000)]], dtype=torch.float)
        features = scaler.scale(features)
        with torch.no_grad():
            pred = model(features)
            age = model(features)
        return {
            "gender": "male" if torch.argmax(pred, 1) == 1 else "female",
            "age": ["teen", "young", "middle", "elderly"][torch.argmax(age, 1)]
        }
    except Exception as e:
        return "bad request" + str(e), 400

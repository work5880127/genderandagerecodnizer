import torch


def evaluate(model, device, test_loader):
    t = [0, 0, 0, 0]
    f = [0, 0, 0, 0]
    fn = [0, 0, 0, 0]
    model.eval()
    with torch.no_grad():
        for batch_idx, data in enumerate(test_loader):
            inputs = data[0].to(device)
            target = data[1].squeeze(1).to(device)

            outputs = model(inputs)
            for res, targ in zip(outputs.argmax(1), target.argmax(1)):
                if res == targ:
                    t[res] += 1
                else:
                    f[res] += 1
                if res != targ:
                    fn[targ] += 1

    return t, f, fn


def accuracy(t: list, f: list):
    return sum(t) / (sum(t) + sum(f))


def precision(t: list, f: list, n):
    return 0 if t[n] == 0 else t[n] / (t[n] + f[n])


def recall(t: list, fn: list, n):
    return 0 if t[n] == 0 else t[n] / (t[n] + fn[n])


def f1(t, f, fn, n):
    p = precision(t, f, n)
    r = recall(t, fn, n)
    return 0 if p == r == 0 else 2 * p * r / (p + r)


def get_metrics(model, device, dev_dataloader):
    t, f, fn = evaluate(model, device, dev_dataloader)
    print("Accuracy: ", accuracy(t, f))
    for i in range(len(t)):
        print("Class ", i)
        print("Precision: ", precision(t, f, i))
        print("Recall: ", recall(t, fn, i))
        print("F1: ", f1(t, f, fn, i))
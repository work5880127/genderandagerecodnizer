import torch
from decouple import config


class Scaler:
    def __init__(self, mean, std):
        self.mean = mean
        self.std = std

    @classmethod
    def from_data(cls, train_data):
        train_data = torch.tensor(train_data.iloc[:, 1:].values)
        mean = train_data.mean(0, keepdim=True)
        std = train_data.std(0, unbiased=False, keepdim=True)
        return cls(mean, std)

    @classmethod
    def from_file(cls, path):
        with open(path) as file:
            text = file.read()
        tensor = torch.tensor  # eval tricks
        text = text.split("sep")
        mean = eval(text[0])
        std = eval(text[1])
        return cls(mean, std)

    def scale(self, data):
        data -= self.mean
        data /= self.std
        return data

    def save(self, path):
        with open(path, "w") as file:
            print(f"{self.mean}", file=file)
            print("sep", file=file)
            print(f"{self.std}", file=file)

    @staticmethod
    def create_scaler(data):
        scaler = Scaler.from_data(data)
        scaler.save(config("GENDER_DATASET") + "/normalizer_config.txt")
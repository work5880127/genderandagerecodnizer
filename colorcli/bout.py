from .colors import bcolors
from time import sleep
from functools import wraps


def _operation(s: str):
    return bcolors.wrap(s, bcolors.BOLD, bcolors.OKCYAN)


def log(text):
    """
    :param text: Text to be displayed
    :return: wrapper

    Use to console log long functions in a colourful way
    """
    @wraps
    def wrapper(func):
        def new(*args, **kwargs):
            print(_operation(text))
            sleep(0.3)  # we want to pause because otherwise there will be no time to draw a plt.plot
            func(*args, **kwargs)

        return func
    return wrapper

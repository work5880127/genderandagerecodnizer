import pandas as pd
from decouple import config
from voice_gender_recognition.colorcli import bcolors


def strip_read_csv(path, **kwargs):
    data = pd.read_csv(path, **kwargs)
    return data.drop(data.keys()[0], axis=1)


def for_all(tp, fnc):
    if tp is None:
        tp = load_all(config("DATASET_LOCATION"), strip=False)

    res = []
    for idx, data in enumerate(tp):
        print(bcolors.wrap("Processing " + ['train', 'test', 'dev'][idx], bcolors.UNDERLINE))
        res.append(fnc(data))
    return res


def load_all(location, strip=True):
    res = []
    for end in ['train', 'test', 'dev']:
        if strip:
            res.append(strip_read_csv(f"{location}/{end}.tsv", sep='\t'))
        else:
            res.append(pd.read_csv(f"{location}/{end}.tsv", sep='\t'))
    return res
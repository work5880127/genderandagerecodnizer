import time

import pandas as pd
from decouple import config
from torch.utils.data import Dataset
import torch
from torchvision.transforms import Lambda
import numpy as np
from tqdm import tqdm

from voice_gender_recognition.colorcli import log
from voice_gender_recognition.preprocess import strip_read_csv, count_data, cleanup_data, load_all, for_all, save_data
from voice_gender_recognition.preprocess.features import create_df_features


class OpenVoiceDataset(Dataset):
    def __init__(self, path, scaler):
        super().__init__()
        self.header = strip_read_csv(path, sep='\t')
        self.label_transform = Lambda(lambda y: torch.zeros(2, dtype=torch.float).scatter_(0, torch.tensor(y), 1.0))
        self.scaler = scaler

    @staticmethod
    def to_num(label):
        convert = {
            "female": 0,
            "male": 1
        }
        return convert[label]

    def __len__(self):
        return len(self.header)

    def __getitem__(self, item):
        label = self.header.iloc[item, 0]
        label = self.to_num(label)
        label = self.label_transform(label)
        data = self.header.iloc[item, 1:]

        # converting to tensor
        data = np.vstack(data.values).astype(np.float32)
        data = torch.from_numpy(data.transpose())
        data = self.scaler.scale(data)

        return data, label


@log("Balancing gender statistics")
def balance_gender(data):
    limit = min(count_data(data)["gender"].values())
    print(f"Leaving {limit} each")
    time.sleep(0.3)

    data_new = pd.DataFrame({"filename": [], "age": [], "gender": []})

    num_female = 0
    num_male = 0
    with tqdm(total=(limit * 2)) as progress:
        for it in data.values:
            if it[2] == "male" and num_male < limit:
                data_new.loc[num_male + num_female] = it
                num_male += 1
                progress.update()
            elif it[2] == "female" and num_female < limit:
                data_new.loc[num_male + num_female] = it
                num_female += 1
                progress.update()
            elif num_male >= limit and num_female >= limit:
                break

    return data_new


def preprocess(cleanup=True):
    if cleanup:
        cleanup_data()
    data = load_all(config("DATASET_HEADERS"))
    data = for_all(data, balance_gender)
    data = for_all(data, create_df_features)
    save_data(config("GENDER_DATASET"), data)
